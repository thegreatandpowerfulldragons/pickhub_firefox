const supportedServices = [
  /*{
    pattern: 'e621.net',
    action: (info, tab) => {
      Send({
        page: info.pageUrl
      })
    }
  }*/
]

let settings = {
  port: 6210
}

function parseJSON(string) {
  try {
    return JSON.parse(string)
  } catch (e) {
    return {}
  }
}

function request(info) {
  info.onDone = info.onDone || (() => {})
  info.onSuccess = info.onSuccess || (() => {})
  info.onFail = info.onFail || (() => {})
  info.method = info.method || 'POST'
  info.action = info.action || ''
  info.port = info.port || settings.port

  let url = `http://127.0.0.1:${info.port}/`

  url += info.action

  if (info.data && info.method == 'GET') {
    url += '?'
    let properties = []
    for (let key in info.data) {
      properties.push(escape(key) + '=' + escape(info.data[key]))
    }
    url += properties.join('&')
  }

  let status = 0

  var xhr = new XMLHttpRequest()

  xhr.open(info.method, url, true)

  if (info.method == 'POST' && info.data) {
    xhr.send(JSON.stringify(info.data))
  } else {
    xhr.send(null)
  }
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4)
    {
      let res = parseJSON(xhr.ResponseText)

      info.onDone(status, res)

      if (xhr.status == 200) {
        info.onSuccess(res)
      } else {
        console.log('Request failed')
        info.onFail()
      }
    }
  }
}

function escapeRegex(string) {
  let result = ''
  for (let char in string) {
    result += '\\' + char
  }
  return result
}

function send(data) {
  console.log('sending data', data)

  request({
    data: data,
    action: 'send',
    onSuccess: (res) => {
      console.log('Data sent')
    },
    onFail: () => {
      console.log('Unable to send data')
    }
  })
}

function checkStatus() {
  request({
    action: 'check',
    onSuccess: () => {
      console.log('Server detected')
    },
    onFail: () => {
      console.log('Unable to detect server')
    }
  })
}

function handle(info, tab)
{
  let found = false
  for (let service in supportedServices) {
    if (RegExp('\\w+://' + escapeRegex(service.pattern) + '/.*').test(info.pageUrl)) {
      service.action(info, tab)
      found = true
    }
  }
  if (!found) {
    send({
      page: info.pageUrl,
      image: info.srcUrl
    })
  }
}

checkStatus()

browser.contextMenus.create({
  id: "send-to-pickhub",
  title: 'Send to PickHub',
  contexts: ["image", "video"]
});

browser.contextMenus.onClicked.addListener(function(info, tab) {
  switch (info.menuItemId) {
    case "send-to-pickhub":
      //console.log(info);
      handle(info, tab);
      break;
  }
})
